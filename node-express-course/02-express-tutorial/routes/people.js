const expres = require('express')
const router = expres.Router()

const { 
    getPeople,
    createPerson,
    createpersonPostman,
    updatePerson,
    deletePerson
} = require('../controllers/people')

// router.get('/',getPeople)

// router.post('/api/people', createPerson)

// router.post('/api/people/postman', createpersonPostman)

// router.put('/:id', updatePerson)

// router.delete('/:id', deletePerson)

router.route('/').get(getPeople).post(createPerson)
router.route('/postman').post(createpersonPostman)
router.route('/:id').put(updatePerson).delete(deletePerson)


module.exports = router